library("sf")
library("dplyr")
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

makeData <- function(){ 
  fillTuple <- function(tuple,val,pop){
    tuple[1] = min(tuple[1],val)
    tuple[2] = max(tuple[2],val)
    tuple[3] = min(tuple[3],val/pop)
    tuple[4] = max(tuple[4],val/pop)
    return(tuple)
  }
  
  makeSFsum <- function(sf){
    sum <- 0
    for (i in 1:nrow(sf)){
      # print(sf[i,]$ID)
      for (j in 1:ncol(sf)){
        if (startsWith(colnames(sf)[j],"Corrected")){
          sum <- sum + sf[[i,j]]
        }
      }
    }
    return(sum)
  }

  #First, we run through every values to get their min and max 
  nbWP = c(100000000,0,100000000,0)
  nbJobs = c(100000000,0,100000000,0)
  nbSchoolerPalce= c(100000000,0,100000000,0)
  nbStudentPalce= c(100000000,0,100000000,0)
  nbPOI = c(100000000,0,100000000,0)
  nbStudents = c(100000000,0,100000000,0)
  nbMinor = c(100000000,0,100000000,0)
  nbAdult = c(100000000,0,100000000,0)
  nbSenior = c(100000000,0,100000000,0)
  HouseholdSize = c(100000000,0,100000000,0)
  PrimaryFluxIn = c(100000000,0,100000000,0)
  PrimaryFluxOut = c(100000000,0,100000000,0)
  SecondaryFluxIn = c(100000000,0,100000000,0)
  Pop = c(10000000000,0,100000000,0)
  data <- data.frame()
  pf <- st_read("/home/mc/Documents/INRIA/donnees/Flux/PrincipalFlux.gpkg")
  sf <- st_read("/home/mc/Documents/INRIA/donnees/Flux/secondaryFlux.gpkg")
  geomArr <- st_read("/home/mc/Documents/INRIA/donnees/INSEE/arrParis.gpkg")
  nbArr <- 20
  for (i in 75101:(75100+nbArr)){
    line <- c(i)
    
    #flux 
    arrG <- filter(geomArr,INSEE_COM == as.character(i))
    sfArr <- st_intersection(sf,arrG)
    nb_SF <- makeSFsum(sfArr)
    
    pfArr <- filter(pf,nameArr == as.character(i))
    nb_PF_in <- pfArr$nSchoolersFromOut + pfArr$nStudentFromOut + pfArr$nWorkersFromOut
    nb_PF_out <- pfArr$nSchoolersGoingOut + pfArr$nStudentGoingOut + pfArr$nWorkersGoingOut
    
    poi <- st_read(paste0("/home/mc/Documents/INRIA/donnees/ParisAll/",i,"/POI.gpkg"))
    nb_POI <- sum(poi$attendanceIndice)
    
    wp <- st_read(paste0("/home/mc/Documents/INRIA/donnees/ParisAll/",i,"/WorkingPlace.gpkg"))
    goodWP <- filter(wp,capacity > 0)
    
    classes <- filter(wp, grepl(".class", typeNameSource, fixed=TRUE))
    primSecClasses <- filter(classes, !grepl("sup.",typeNameSource, fixed = TRUE))
    supClasses <- filter(classes, grepl("sup.",typeNameSource, fixed = TRUE))
    
    nb_Schooler_Places <- sum(primSecClasses$capacity)
    nb_Student_Places <- sum(supClasses$capacity)
    
    nb_WP <- nrow(filter(goodWP, !grepl(".class", typeNameSource, fixed=TRUE)))
    capa_WP <- sum(filter(goodWP, !grepl(".class", typeNameSource, fixed=TRUE))$capacity)
    
    
    ind <- st_read(paste0("/home/mc/Documents/INRIA/donnees/ParisAll/",i,"/Individual.gpkg"))
    nb_Students <- nrow(filter(ind,occupation == "STUDENT"))
    nb_Minor <- nrow(filter(ind, age < 18))
    nb_Adult <- nrow(filter(ind, (age >= 18) & (age < 60) ))
    nb_Senior <- nrow(filter(ind, age >=  60))
    
    hh <- st_read(paste0("/home/mc/Documents/INRIA/donnees/ParisAll/",i,"/Household.gpkg"))
    meanHS <- mean(hh$HouseholdSize)
    
    # put vals into line 
    line <- append(line,nb_POI)
    line <- append(line,nb_WP)
    line <- append(line,capa_WP)
    line <- append(line,nb_Schooler_Places)
    line <- append(line,nb_Student_Places)
    line <- append(line,nb_Students)
    line <- append(line,nb_Minor)
    line <- append(line,nb_Adult)
    line <- append(line,nb_Senior)
    line <- append(line,meanHS)
    line <- append(line,nb_SF)  
    line <- append(line,nb_PF_in)  
    line <- append(line,nb_PF_out)  
    #min-max tuples     
    pop <-  nb_Minor + nb_Adult + nb_Senior
    line <- append(line,pop)
    Pop <- fillTuple(Pop, pop,pop)
    HouseholdSize <- fillTuple(HouseholdSize, meanHS,1)
    nbSenior <- fillTuple(nbSenior,nb_Senior,pop)
    nbMinor <- fillTuple(nbMinor,nb_Minor,pop)
    nbStudents <- fillTuple(nbStudents, nb_Students,pop)
    nbPOI <- fillTuple(nbPOI, nb_POI,pop)
    nbAdult <- fillTuple(nbAdult,nb_Adult,pop)
    nbJobs <- fillTuple(nbJobs, capa_WP,pop)
    nbSchoolerPalce <- fillTuple(nbSchoolerPalce, nb_Schooler_Places, pop)
    nbStudentPalce <- fillTuple(nbStudentPalce, nb_Student_Places, pop)
    nbWP <- fillTuple(nbWP, nb_WP,pop)
    SecondaryFluxIn <- fillTuple(SecondaryFluxIn, nb_SF, pop)
    PrimaryFluxIn <- fillTuple(PrimaryFluxIn, nb_PF_in, pop)
    PrimaryFluxOut <- fillTuple(PrimaryFluxOut, nb_PF_out, pop)
    data <- rbind(data,as.numeric(line))
  }
  header <- c("Arrondissement","Nb POI", "Nb Lieux de travail" , "Nb Jobs" ,"Nb de places d'école", "Nb de places universitaires", "Nb étudiants" , "Nb mineurs", "Nb adultes" , "Nb seniors" , "Moyenne taille ménage", "Flux secondaire entrant","Flux primaire entrant","Flux primaire sortant", "Population")
  colnames(data) <- header 
  percentage <- data.frame()
  percentageNorm <- data.frame()
  
  minMax <- data.frame(i=c(1:4), nbPOI, nbWP, nbJobs, nbSchoolerPalce, nbStudentPalce, nbStudents, nbMinor, nbAdult, nbSenior, HouseholdSize, SecondaryFluxIn, PrimaryFluxIn, PrimaryFluxOut)

    #Make percentages and ratios
  for (j in 1:nbArr){
    line <- c(data[[1]][[j]]) #write arr number
    lineNorm <- c(data[[1]][[j]])
    for (i in 2:(ncol(data)-1)){ # for the other columns with data
      # lineNorm <- append(line, ((data[[i]][[j]] - minMax[[i]][1]) / (minMax[[i]][2] - minMax[[i]][1]))*100)
      line <- append(line, ((data[[i]][[j]] - minMax[[i]][1]) / (minMax[[i]][2] - minMax[[i]][1]))*100)
      if (i != 11){ #10th line is a mean, we dont divide it by population
        lineNorm <- append(lineNorm, ((data[[i]][[j]]/data[[ncol(data)]][[j]] - minMax[[i]][3]) / (minMax[[i]][4] - minMax[[i]][3]))*100)
      } else {
        lineNorm <- append(lineNorm, ((data[[i]][[j]] - minMax[[i]][1]) / (minMax[[i]][2] - minMax[[i]][1]))*100)
      }
    }
    percentage <- rbind(percentage,as.numeric(line))
    percentageNorm <- rbind(percentageNorm,as.numeric(lineNorm))
  }
  colnames(percentage) <- header[1:(length(header)-1)]
  colnames(percentageNorm) <- header[1:(length(header)-1)]
  percentage$Arrondissement <- paste0((percentage$Arrondissement - 75100),"ème")
  percentageNorm$Arrondissement <- paste0((percentageNorm$Arrondissement - 75100),"ème")
  
  # Try to add color 
  library(RColorBrewer)
  n <- 20
  qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
  col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
  percentage$color <- paste0(sample(col_vector, n),"89")
  pie(rep(1,n), col=percentage$color)
  percentageNorm$color <- percentage$color

  write.csv(percentage, "percentage.csv",row.names = FALSE)
  write.csv(percentageNorm, "percentageNorm.csv",row.names = FALSE)
  
}

if (!file.exists("percentage.csv")){
  makeData()
} else {
  percentage <- read.csv("percentage.csv")
  percentageNorm <- read.csv("percentageNorm.csv")
}

library(fmsb)
library(plotly)

# # To use the fmsb package, I have to add 2 lines to the dataframe: the max and min of each topic to show on the plot!
# d <- rbind(c(0,nbPOI[2],nbWP[2],capaWP[2],nbStudents[2],nbMinor[2],nbAdult[2],nbSenior[2],HouseholdSize[2]) , c(1,nbPOI[1],nbWP[1],capaWP[1],nbStudents[1],nbMinor[1],nbAdult[1],nbSenior[1],HouseholdSize[1])  , data)

# Check your data, it has to look like this!
# head(data)

# The default radar chart 
radarchart(percentage[1:3,2:9])
p <- radarchart( percentage[1:3,2:9]  , axistype=1 , 
                 pcol=rgb(0.2,0.5,0.5,0.9) , pfcol=rgb(0.2,0.5,0.5,0.5) , plwd=4 , 
                 cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,100,8), cglwd=0.8,
                 vlcex=0.8 
)

fig <- plot_ly(
  type = 'scatterpolar',
  fill = 'toself'
) 
arr <- 14
fig <- fig %>%
  add_trace( 
    type="scatterpolar",
    #data = percentage[arr,2:9],
    r = unlist(percentage[arr,2:9]),
    theta = names(percentage[2:9]),
    name = as.character(percentage[[1]][[arr]])
  ) 
# arr <- 15
# fig <- fig %>%
#   add_trace( 
#     type="scatterpolar",
#     #data = percentage[arr,2:9],
#     r = unlist(percentage[arr,2:9]),
#     theta = names(percentage[2:9]),
#     name = as.character(percentage[[1]][[arr]])
#   ) 


fig <- fig %>%
  layout(
    polar = list(
      radialaxis = list(
        visible = T,
        range = range(0,100)
        )
      )
  )
fig






# App

library(shiny)

ui <- fluidPage(
  tags$head(
  # Application title
  tags$link(rel = "icon", type = "image/png", sizes = "32x32", href = "logo_small_ICI.png")),
  titlePanel( title = 'Caractéristiques des arrondissements de Paris', windowTitle = "ICI data: Arrondissements Paris" ), 


  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
      radioButtons("type",
                   "Données représentées",
                   choices = c("Normalisé par la population de l'arrondissement","En valeur absolue"),
                   selected = "Normalisé par la population de l'arrondissement"),
      checkboxGroupInput("arr",
                   "Arrondissement à visualiser :",
                   choices = paste0(c(1:20),"ème"),
                   selected = "5ème")
    ),
    
    # Show a plot of the generated distribution
    mainPanel(
      h3("Caractéristiques générales"),
      plotlyOutput(outputId = "gen"),
      fluidRow(
        column(4, h4("Structure urbaine"), plotlyOutput(outputId = "urb")),
        column(4, h4("Population"), plotlyOutput(outputId = "pop")),
        column(4, h4("Flux"), plotlyOutput(outputId = "flux")),
      )
    )
  )
)


server <- function(input, output) {
  perc <- reactive({
    collec <- data.frame()
    if (input$type == "En valeur absolue"){
      collec <- percentage 
    }
    else{
      collec <- percentageNorm
    }
    if (length(input$arr) == 0){
      # data.frame(rep(times = ncol(percentage),NA))
      data.frame("Arrondissement" =NA,"Nb POI" =NA, "Nb Lieux de travail" =NA , "Nb Jobs" =NA ,"Nb de places d'école" =NA, "Nb de places universitaires" =NA, "Nb étudiants" =NA , "Nb mineurs" =NA, "Nb adultes" =NA , "Nb seniors" =NA , "Moyenne taille ménage" =NA, "Flux secondaire entrant"=NA,"Flux primaire entrant"=NA,"Flux primaire sortant"=NA)
    }else{
    subset(
      collec,
      Arrondissement %in% input$arr
    )
    }
  }
    )
  
  output$gen <- renderPlotly({
    # print(typeof(input$arr))
    # print(input$arr)
    # print(perc())
    # for (iii in 1:nrow(perc())){
    #   print(perc()[iii,2:9])
    #   typeof(perc()[iii,2:9])
    # }
    fig <- plot_ly(type = 'scatterpolar', fill = 'toself')
    
    for (iii in 1:nrow(perc())){
      fig <- fig %>%
        add_trace(
          type="scatterpolar",
          #data = percentage[arr,2:11],
          r = unlist(perc()[iii,2:14]),
          theta = names(perc()[iii,2:14]),
          name = as.character(paste0(perc()[iii,1]),"ème"),
          fillcolor = perc()[iii,"color"],
          marker = list(color = perc()[iii,"color"])
        )
    }
    fig <- fig %>%
      layout(
        polar = list(
          radialaxis = list(
            visible = T,
            range = range(0,100)
          )
        )
      )
  })
  output$urb <- renderPlotly({
    fig <- plot_ly(type = 'scatterpolar', fill = 'toself')
    
    for (iii in 1:nrow(perc())){
      fig <- fig %>%
        add_trace(
          type="scatterpolar",
          #data = percentage[arr,2:11],
          r = unlist(perc()[iii,2:6]),
          theta = names(perc()[iii,2:6]),
          name = as.character(paste0(perc()[iii,1]),"ème"),
          fillcolor = perc()[iii,"color"],
          marker = list(color = perc()[iii,"color"])
        )
    }
    fig <- fig %>%
      layout(
        polar = list(
          radialaxis = list(
            visible = T,
            range = range(0,100)
          )
        )
      )
  })
  output$pop <- renderPlotly({
    fig <- plot_ly(type = 'scatterpolar', fill = 'toself')
    
    for (iii in 1:nrow(perc())){
      fig <- fig %>%
        add_trace(
          type="scatterpolar",
          #data = percentage[arr,2:11],
          r = unlist(perc()[iii,7:11]),
          theta = names(perc()[iii,7:11]),
          name = as.character(paste0(perc()[iii,1]),"ème"),
          fillcolor = perc()[iii,"color"],
          marker = list(color = perc()[iii,"color"])
        )
    }
    fig <- fig %>%
      layout(
        polar = list(
          radialaxis = list(
            visible = T,
            range = range(0,100)
          )
        )
      )
  })
  output$flux <- renderPlotly({
    fig <- plot_ly(type = 'scatterpolar', fill = 'toself')
    for (iii in 1:nrow(perc())){
      fig <- fig %>%
        add_trace(
          type="scatterpolar",
          #data = percentage[arr,2:11],
          r = unlist(perc()[iii,12:14]),
          theta = names(perc()[iii,12:14]),
          name = as.character(paste0(perc()[iii,1]),"ème"),
          fillcolor = perc()[iii,"color"],
          marker = list(color = perc()[iii,"color"])
        )
    }
    fig <- fig %>%
      layout(
        polar = list(
          radialaxis = list(
            visible = T,
            range = range(0,100)
            )
        )
      )
  })
}


# Run the application 
shinyApp(ui = ui, server = server)

